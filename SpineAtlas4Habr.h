#ifndef HEADER_SPINEATLAS_H
#define HEADER_SPINEATLAS_H

struct spAtlas;

class SpineAtlas
{
public:
	SpineAtlas(const std::string& name);
	~SpineAtlas();

private:
	std::string		mName;
	spAtlas*		mAtlas;
};

#endif // HEADER_SPINEATLAS_H
