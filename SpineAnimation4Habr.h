#ifndef HEADER_SPINEANIMATION_H
#define HEADER_SPINEANIMATION_H

class SpineAtlas;
struct spSkeleton;
struct spAnimation;
struct spAimationState;
struct spAnimationStateData;
struct spSlot;
struct spRegionAttachment;
struct spEvent;

class SpineAnimation
{
public:
	SpineAnimation(const std::string& name, float scale);
	~SpineAnimation();

	void			Update(float timeElapsed);
	void			Render();

	void			Play(const std::string& skin, const std::string& animation, bool looped);
	void			Stop();

	void			OnAnimationEvent(SpineAnimationState* state, int trackIndex, int type, spEvent* event, int loopCount);

	float			GetAnimationTime(const std::string& name) const;

private:
	spAnimation*		GetAnimation(const std::string& name) const;
	void			FillSlotVertices(Vertex* points, float x, float y, spSlot* slot, spRegionAttachment* attachment);

	std::string		mName;
	std::string		mCurrentAnimation;
	SpineAtlas*		mAtlas;
	spAnimationState*	mState;
	spAnimationStateData*	mStateData;
	spSkeleton*		mSkeleton;
	float			mSpeed;
	bool			mPlaying;
	Vec2			mPosition;
	bool			mFlipX;
};

#endif // HEADER_SPINEANIMATION_H
