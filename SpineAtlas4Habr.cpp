#include "Engine.h"
#include "SpineAtlas.h"
#include "spine.h"

SpineAtlas::SpineAtlas(const std::string& name) : 
	mName(name), mAtlas(0)
{
	int length = 0;
	const char* data = (const char*)store::Load(name + ".atlas", length); 
	if (data)
	{
		mAtlas = spAtlas_create(data, length, "", 0);
		store::Free(name + ".atlas");
	}
}

SpineAtlas::~SpineAtlas()
{
    spAtlas_dispose(mAtlas);
}

////////////////////////////////////////////////////////////////////////////////////////

extern "C" void _spAtlasPage_createTexture(spAtlasPage* self, const char* path)
{
	Texture* texture = textures::LoadTexture(path);
	self->width = texture->width;
	self->height = texture->height;
	self->rendererObject = texture;
}

extern "C" void _spAtlasPage_disposeTexture(spAtlasPage* self)
{
	Texture* texture = (Texture*)self->rendererObject;
	render::ReleaseTexture(texture);
}
