#include "SpineAnimation.h"
#include "SpineAtlas.h"
#include "spine.h"
#include "extension.h"

SpineAnimation::SpineAnimation(const std::string& name, float scale) : 
	mName(name), mAtlas(0), mState(0), mStateData(0), mSkeleton(0), mSpeed(1), mPlaying(false), mFlipX(false)
{
	mAtlas = gAnimationHost.GetAtlas(mName);

	spSkeletonJson* skeletonJson = spSkeletonJson_create(mAtlas->GetAtlas());
	skeletonJson->scale = scale;
	spSkeletonData* skeletonData = spSkeletonJson_readSkeletonDataFile(skeletonJson, (name + ".json").c_str());
	assert(skeletonData);
	spSkeletonJson_dispose(skeletonJson);

	mSkeleton = spSkeleton_create(skeletonData);
	mStateData = spAnimationStateData_create(skeletonData);
	mState = spAnimationState_create(mStateData);
	mState->rendererObject = this;
	spSkeleton_update(mSkeleton, 0);
	spAnimationState_update(mState, 0);
	spAnimationState_apply(mState, mSkeleton);
	spSkeleton_updateWorldTransform(mSkeleton);
}

SpineAnimation::~SpineAnimation()
{
	spAnimationState_dispose(mState);
	spAnimationStateData_dispose(mStateData);
	spSkeleton_dispose(mSkeleton);
}

void SpineAnimation::Update(float timeElapsed)
{
	if (IsPlaying())
	{
		mSkeleton->flipX = mFlipX;
		spSkeleton_update(mSkeleton, timeElapsed * mSpeed / 1000); // timeElapsed - ms, spine ���������� ����� � ��������
		spAnimationState_update(mState, timeElapsed * mSpeed / 1000);
		spAnimationState_apply(mState, mSkeleton);
		spSkeleton_updateWorldTransform(mSkeleton);
	}
}

void SpineAnimation::Render()
{
	int slotCount = mSkeleton->slotCount; 
	Vertex vertices[6];
	for (int i = 0; i < slotCount; ++i) 
	{
		spSlot* slot = mSkeleton->slots[i];
		spAttachment* attachment = slot->attachment;
		if (!attachment || attachment->type != SP_ATTACHMENT_REGION) 
			continue;
		spRegionAttachment* regionAttachment = (spRegionAttachment*)attachment;
		FillSlotVertices(vertices], mPosition.x, mPosition.y, slot, regionAttachment);
		texture = (Texture*)((spAtlasRegion*)regionAttachment->rendererObject)->page->rendererObject;
		render::BindTexture(texture);
		render::DrawVertexArray(vertices, 6);
	}
}

void SpineAnimation::FillSlotVertices(Vertex* points, float x, float y, spSlot* slot, spRegionAttachment* attachment)
{
	Color color(mSkeleton->r * slot->r, mSkeleton->g * slot->g, mSkeleton->b * slot->b, mSkeleton->a * slot->a);
	points[0].c = points[1].c = points[2].c = points[3].c = points[4].c = points[5].c = color;
	points[0].uv.x = points[5].uv.x = attachment->uvs[SP_VERTEX_X1];
	points[0].uv.y = points[5].uv.y = attachment->uvs[SP_VERTEX_Y1];
	points[1].uv.x = attachment->uvs[SP_VERTEX_X2];
	points[1].uv.y = attachment->uvs[SP_VERTEX_Y2];
	points[2].uv.x = points[3].uv.x = attachment->uvs[SP_VERTEX_X3];
	points[2].uv.y = points[3].uv.y = attachment->uvs[SP_VERTEX_Y3];
	points[4].uv.x = attachment->uvs[SP_VERTEX_X4];
	points[4].uv.y = attachment->uvs[SP_VERTEX_Y4];
	float* offset = attachment->offset;
	float xx = slot->skeleton->x + slot->bone->worldX;
	float yy = slot->skeleton->y + slot->bone->worldY;
	points[0].xyz.x = points[5].xyz.x = x + xx + offset[SP_VERTEX_X1] * slot->bone->m00 + offset[SP_VERTEX_Y1] * slot->bone->m01;
	points[0].xyz.y = points[5].xyz.y = y - yy - (offset[SP_VERTEX_X1] * slot->bone->m10 + offset[SP_VERTEX_Y1] * slot->bone->m11);
	points[1].xyz.x = x + xx + offset[SP_VERTEX_X2] * slot->bone->m00 + offset[SP_VERTEX_Y2] * slot->bone->m01;
	points[1].xyz.y = y - yy - (offset[SP_VERTEX_X2] * slot->bone->m10 + offset[SP_VERTEX_Y2] * slot->bone->m11);
	points[2].xyz.x = points[3].xyz.x = x + xx + offset[SP_VERTEX_X3] * slot->bone->m00 + offset[SP_VERTEX_Y3] * slot->bone->m01;
	points[2].xyz.y = points[3].xyz.y = y - yy - (offset[SP_VERTEX_X3] * slot->bone->m10 + offset[SP_VERTEX_Y3] * slot->bone->m11);
	points[4].xyz.x = x + xx + offset[SP_VERTEX_X4] * slot->bone->m00 + offset[SP_VERTEX_Y4] * slot->bone->m01;
	points[4].xyz.y = y - yy - (offset[SP_VERTEX_X4] * slot->bone->m10 + offset[SP_VERTEX_Y4] * slot->bone->m11);
}

void SpineAnimationStateListener(spAnimationState* state, int trackIndex, spEventType type, spEvent* event, int loopCount)
{
	SpineAnimation* sa = (SpineAnimation*)state->rendererObject;
	if (sa)
		sa->OnAnimationEvent((SpineAnimationState*)state, trackIndex, type, event, loopCount);
}

void SpineAnimation::Play(const std::string& skinName, const std::string& animationName, bool looped)
{
	if (mCurrentAnimation == animationName)
		return;

	spAnimation* animation = GetAnimation(animationName);  
	if (animation)
	{
		mCurrentAnimation = animationName;	   
		mSpeed = 1.f;

		if (!skinName.empty())
		{
			spSkeleton_setSkinByName(mSkeleton, skinName.c_str()); 
			spSkeleton_setSlotsToSetupPose(mSkeleton);
		}

		spTrackEntry* entry = spAnimationState_setAnimation(mState, 0, animation, looped);
		if (entry)
			entry->listener = SpineAnimationStateListener;
		mPlaying = true;
	}
	else
		Stop();
}

void SpineAnimation::Stop()
{
	mCurrentAnimation.clear();   
	mPlaying = false;
}

spAnimation* SpineAnimation::GetAnimation(const std::string& name) const
{
	return spSkeletonData_findAnimation(mSkeleton->data, name.c_str());
}

float SpineAnimation::GetAnimationTime(const std::string& name) const
{
	spAnimation* ani = GetAnimation(name);
	return ani ? ani->duration * 1000 : 0; // s -> ms;
}

void SpineAnimation::OnAnimationEvent(SpineAnimationState* state, int trackIndex, int type, spEvent* event, int loopCount)
{
	spTrackEntry* entry = spAnimationState_getCurrent(state, trackIndex);
	if (entry && !entry->loop && type == SP_ANIMATION_COMPLETE)
		Stop();
}

///////////////////////////////////////////////////////////////////////////////////////

extern "C" char* _spUtil_readFile(const char* path, int* length)
{
	char* result = 0;
	const void* buffer = store::Load(path, *length);
	if (buffer)
	{
		result = (char*)_malloc(*length, __FILE__, __LINE__); // spine malloc
		memcpy(result, buffer, *length);
		store::Free(path);
	}
	return result;
}

